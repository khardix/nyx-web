"""Generic utilities"""

from itertools import chain, zip_longest
from typing import Iterable, Iterator, Sequence

MERGE_SENTINEL = object()


def merge(*iterable_seq: Sequence[Iterable], longest: bool = False) -> Iterator:
    """Merge several iterables into one.

    Yields first items from all iterables in sequence,
    then second items, then third items, etc.

    Arguments: The iterables to merge.
    Keyword arguments:
        longest: Whether to end once the shortest iterable is exhausted (False)
            or when the longest iterable is exhausted (True).
            If True, missing items are skipped.
    """

    if longest:
        zipped = zip_longest(*iterable_seq, fillvalue=MERGE_SENTINEL)
    else:
        zipped = zip(*iterable_seq)

    chained = chain.from_iterable(zipped)
    return filter(lambda item: item is not MERGE_SENTINEL, chained)
