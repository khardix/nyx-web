"""Calendar-related utilities"""

from datetime import date


def shortcut_format(start: date, end: date) -> str:
    """Determine shortened format for date span accordint to Czech customs.

    Arguments:
        start: Start date of the date span.
        end: End date of the date span.

    Returns:
        `Unicode CLDR date pattern <http://cldr.unicode.org/translation/date-time-1/date-time-patterns>`_.
        In case the dates are the same, the pattern will be an empty string.
    """

    if start.year != end.year:
        return "d. MMMM y"  # long
    elif start.month != end.month:
        return "d. MMMM"
    elif start.day != end.day:
        return "d."
    else:
        return ""
