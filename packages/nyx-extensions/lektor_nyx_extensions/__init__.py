"""Site-specific extensions"""

from lektor.pluginsystem import Plugin

from . import calendar, util


class NyxExtensionsPlugin(Plugin):
    name = "Nyx Extensions"
    description = "Various extensions used by the Nyx website."

    def on_setup_env(self, **_extra):
        """Modify Jinja environment."""

        self.env.jinja_env.globals.update(
            merged=util.merge,
            shortdatefmt=calendar.shortcut_format,
        )
